---
title: Home
nav_order: 1
description: "The Git example created in the tutorial."
permalink: /
---

# Git Course example

This is the `git` course created as an example in the **Courseware as Code** tutorial!
