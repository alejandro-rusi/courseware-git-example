# courseware-site-template

This repository contains a template for **Courseware as Code** generating a static website using Jekyll that can be deployed to Gitlab Pages for free.

This project started out as a fork of the amazing Jekyll template (just-the-docs)[https://pmarsceill.github.io/just-the-docs/]. Courseware as Code wouldn't be possible without it.

.
